import random
from django.http import HttpResponse

def hi(request):
    num = random.randrange(0, 1000)
    text = f'<H1>Hello Goguma,  your lucky number is {num}</H1>'
    return HttpResponse(text)


def goguma_hi(request):
    num = random.randrange(0, 1000)
    text = f'<H1>Hi, Goguma  your lucky number is {num}</H1>'
    return HttpResponse(text)


def fallback(request):
    text = f'<H1>Goguma Fall back</H1>'
    return HttpResponse(text)